#include "PowerSensor.h"

namespace powersensor {

    PowerSensor::~PowerSensor() {};

    double PowerSensor::seconds(
        const State &firstState,
        const State &secondState)
    {
       return secondState.timeAtRead -
              firstState.timeAtRead;
    }

    double PowerSensor::Joules(
       const State &firstState,
       const State &secondState)
    {
        return secondState.joulesAtRead -
                firstState.joulesAtRead;
    }

    double PowerSensor::Watt(
        const State &firstState,
        const State &secondState)
    {
        return Joules(firstState, secondState) /
               seconds(firstState, secondState);
    }

    void PowerSensor::dump(
        const char *dumpFileName)
    {
        if (dumpFileName) {
            dumpFile = std::unique_ptr<std::ofstream>(new std::ofstream(dumpFileName));
        }
    }

    void PowerSensor::dump(
        const State &startState,
        const State &firstState,
        const State &secondState)
    {
        #if (ENABLE_DUMP)
        if (dumpFile != nullptr) {
            std::unique_lock<std::mutex> lock(dumpFileMutex);
            *dumpFile << "S " << seconds(startState, secondState)
                      << " " << Watt(firstState, secondState)
                      << std::endl;
        }
        #endif
    }

    void PowerSensor::mark(
        const State &startState,
        const State &currentState,
        const char *name,
        unsigned tag) const
    {
        #if ENABLE_DUMP
        if (dumpFile != nullptr) {
            std::unique_lock<std::mutex> lock(dumpFileMutex);
            *dumpFile << "M " << currentState.timeAtRead - startState.timeAtRead
                      << ' ' << tag << " \""
                      << (name == nullptr ? "" : name)
                      << '"' << std::endl;
        }
        #endif
    }

} // end namespace powersensor
