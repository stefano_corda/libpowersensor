project(powersensor-common)

add_library(
    ${PROJECT_NAME}
    OBJECT
    PowerSensor.cpp
)

set_target_properties(
    ${PROJECT_NAME}
    PROPERTIES
    COMPILE_FLAGS "-fPIC"
)

install(
    FILES
    PowerSensor.h
    DESTINATION
    include/powersensor
)
