#ifndef POWER_SENSOR_H_
#define POWER_SENSOR_H_

#include "powersensor-config.h"

#include <memory> // unique_ptr
#include <mutex>
#include <fstream>

namespace powersensor {

    class State {
        public:
            double timeAtRead;
            double joulesAtRead;
    };

    class PowerSensor {
        public:
            virtual ~PowerSensor() = 0;

            virtual State read() = 0;

            static double seconds(
                const State &firstState,
                const State &secondState);

            static double Joules(
               const State &firstState,
               const State &secondState);

            static double Watt(
                const State &firstState,
                const State &secondState);

            void dump(
                const char *dumpFileName);

            void mark(
                const State &startState,
                const State &currentState,
                const char *name = 0,
                unsigned tag = 0) const;

        protected:
            void dump(
                const State &startState,
                const State &firstState,
                const State &secondState);

        private:
            std::unique_ptr<std::ofstream> dumpFile = nullptr;
            mutable std::mutex dumpFileMutex;

    };
} // end namespace powersensor

#endif
