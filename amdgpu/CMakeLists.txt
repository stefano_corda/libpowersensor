project(powersensor-amdgpu)

add_library(
    ${PROJECT_NAME}
    OBJECT
    AMDGPUPowerSensor.cpp
)

set_target_properties(
    ${PROJECT_NAME}
    PROPERTIES
    COMPILE_FLAGS "-fPIC"
)

install(
    FILES
    AMDGPUPowerSensor.h
    DESTINATION
    include/powersensor
)
