#include <unistd.h>
#include <omp.h>
#include <dirent.h>
#include <string.h>

#include <cstdio>
#include <cstdlib>
#include <string>

#include <sstream>
#include <vector>
#include <iostream>

#include "AMDGPUPowerSensor.h"

namespace powersensor {
namespace amdgpu {

class AMDGPUPowerSensor_ : public AMDGPUPowerSensor {
    public:
        AMDGPUPowerSensor_(const unsigned device_number, const char *dumpFileName);
        ~AMDGPUPowerSensor_();

        State read();
        void mark(const State &, const char *name = 0, unsigned tag = 0);
        void mark(const State &start, const State &stop, const char *name = 0, unsigned tag = 0);

    private:
        std::string filename;
        pthread_t       thread;
        volatile bool   stop;
        static void     *IOthread(void *);
        void            *IOthread();
};

AMDGPUPowerSensor* AMDGPUPowerSensor::create(
    const unsigned device_number,
    const char *dumpFileName)
{
    return new AMDGPUPowerSensor_(device_number, dumpFileName);
}


AMDGPUPowerSensor_::AMDGPUPowerSensor_(const unsigned device_number, const char *dumpFileName) :
    stop(false)
{
	// Power consumption is read from sysfs
	const char *dri_dir = "/sys/kernel/debug/dri";

	// Vector for all files (all GPUs)
	std::vector<std::string> pmfiles;

	// Try to find all AMD GPUs
	std::stringstream basedir;
	basedir << dri_dir << "/";
    DIR *d = opendir(basedir.str().c_str());
    struct dirent *dir;
    if (d) {
        while ((dir = readdir(d)) != NULL) {
			std::stringstream pmfile;
			pmfile << basedir.str() << dir->d_name << "/amdgpu_pm_info";

   			if (fopen(pmfile.str().c_str(), "r")) {
				pmfiles.push_back(pmfile.str());
			}
        }
        closedir(d);
    } else {
		fprintf(stderr, "Could not open directory: %s\n", dri_dir);
	}

    // Select the file to read power consumption from
    filename = pmfiles[device_number];

    State startState = read();

    if ((errno = pthread_create(&thread, 0, &AMDGPUPowerSensor_::IOthread, this)) != 0) {
        perror("pthread_create");
        exit(1);
    }
}

AMDGPUPowerSensor_::~AMDGPUPowerSensor_() {
    stop = true;

    if ((errno = pthread_join(thread, 0)) != 0) {
        perror("pthread_join");
    }
}


void *AMDGPUPowerSensor_::IOthread(void *arg) {
    return static_cast<AMDGPUPowerSensor_ *>(arg)->IOthread();
}


void *AMDGPUPowerSensor_::IOthread() {
    State firstState = read(), currentState = firstState, previousState;

    while (!stop) {
        usleep(100);
        previousState = currentState;
        currentState  = read();
        // TODO: dump crashes
        // dump(firstState, previousState, currentState);
    }
}

float get_power(
	std::string &filename)
{
	FILE *file = fopen(filename.c_str(), "r");
	char line[80];
	while (fgets(line, sizeof(line), file) != NULL) {
		if (strstr(line, "average GPU") != NULL) {
			const char delim[2] = " ";
			const char *token = strtok(line, delim);
            fclose(file);
			return atof(token);
		}
    }
    fclose(file);
	return -1;
}

State AMDGPUPowerSensor_::read() {
    State state;
    state.timeAtRead = omp_get_wtime();

    #pragma omp critical (power)
    {
        state.joulesAtRead = get_power(filename);
    }

    return state;
}

} // end namespace amdgpu
} // end namespace powersensor
